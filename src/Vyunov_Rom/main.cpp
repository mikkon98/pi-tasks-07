#include <vector>
#include <iostream>
#include <fstream>
//#include <string>
#include "Employee.h"

using namespace std;

int main() {
	vector<Employee*> employee;
	ifstream fcin("employee.txt");
	int id;
	while (fcin >> id) 
	{
		string name, surname, patronymic;
		fcin >> name >> surname >> patronymic;
		string position;
		fcin >> position;
		int worktime;
		fcin >> worktime;
		if (position == "Cleaner") {
			int base;
			fcin >> base;
			employee.push_back(new Cleaner(id, name + " " + surname + " " + patronymic, base, worktime));
		}
		if (position == "Driver") {
			int base;
			fcin >> base;
			employee.push_back(new Driver(id, name + " " + surname + " " + patronymic, base, worktime));
		}
		if (position == "Tester") {
			int base;
			fcin >> base;
			int project;
			fcin >> project;
			double contribution;
			fcin >> contribution;
			employee.push_back(new Tester(id, name + " " + surname + " " + patronymic, worktime, base, project, contribution));
		}
		if (position == "Programmer") {
			int base;
			fcin >> base;
			int project;
			fcin >> project;
			double contribution;
			fcin >> contribution;
			employee.push_back(new Programmer(id, name + " " + surname + " " + patronymic, worktime, base, project, contribution));
		}
		if (position == "TeamLeader") {
			int base;
			fcin >> base;
			int quality;
			fcin >> quality;
			int project;
			fcin >> project;
			double contribution;
			fcin >> contribution;
			employee.push_back(new TeamLeader(id, name + " " + surname + " " + patronymic, worktime, base, project, 
				 contribution, quality));
		}
		if (position == "Manager") {
			int project;
			fcin >> project;
			double contribution;
			fcin >> contribution;
			employee.push_back(new Manager(id, name + " " + surname + " " + patronymic, worktime, project, contribution));
		}
		if (position == "ProjectManager") {
			int quality;
			fcin >> quality;
			int project;
			fcin >> project;
			double contribution;
			fcin >> contribution;
			employee.push_back(new ProjectManager(id, name + " " + surname + " " + patronymic, worktime, project, contribution, quality));
		}
		if (position == "SeniorManager") {
			int quality;
			fcin >> quality;
			int project;
			fcin >> project;
			int contribution;
			fcin >> contribution;
			employee.push_back(new SeniorManager(id, name + " " + surname + " " + patronymic, worktime, project, contribution, quality));
		}
	}
	for (int i = 0; i < employee.size(); ++i) {
		employee[i]->SetPayment();
		cout << employee[i]->GetId() << "\t" << employee[i]->GetFio() << "\t" << employee[i]->GetPayment() << "\n";
	}
	return 0;
}