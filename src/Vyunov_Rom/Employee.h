#include <string>
using namespace std;

class WorkTime //���������
{
public:
	virtual int CalcBasePay() = 0;
};

class Project //���������
{
public:
	virtual int CalcProjPay() = 0;
};

class Heading //���������
{
public:
	virtual int CalcHeadPay() = 0;
};

class Employee
{
protected:
	int ID;
	string FIO;
	string Position;
	int Work_Unit;
	int Payment;
public:
	Employee(int ID, string FIO, int Work_Unit) : ID(ID), Work_Unit(Work_Unit), Payment(0), FIO(FIO){};
	void SetPayment()
	{
		Payment = CalcPay();
	}
	int GetId()
	{
		return ID;
	}
	string GetFio()
	{
		return FIO;
	}
	int GetPayment()
	{
		return Payment;
	}
	virtual int CalcPay() = 0;
};

class Personal :public WorkTime, public Employee
{
protected:
	int base;
public:
	Personal(int ID, string Name, int base, int Work_Unit) :Employee(ID, Name, Work_Unit), base(base) {};
	int CalcPay()
	{
		return CalcBasePay();
	}
	int CalcBasePay()
	{
		return Work_Unit*base;
	}
};

class Engineer :public WorkTime, public Employee, public Project
{
protected:
	int base;
	float ProjectPart;
	int ProjBudjet;
public:
	Engineer(int ID, string FIO, int base, int Work_Unit, int ProjBudjet, float ProjectPart) :Employee(ID, FIO, Work_Unit),
		base(base), ProjBudjet(ProjBudjet), ProjectPart(ProjectPart) {};
	int CalcPay()
	{
		return CalcBasePay() + CalcProjPay();
	}
	int CalcBasePay()
	{
		return Work_Unit*base;
	}
	int CalcProjPay()
	{
		return ProjBudjet*ProjectPart;
	}
};

class Manager :public Employee, public Project
{
protected:
	float ProjectPart;
	int ProjBudjet;
public:
	Manager(int ID, string FIO, int Work_Unit, int ProjBudjet, float ProjectPart) :Employee(ID, FIO, Work_Unit),
		ProjBudjet(ProjBudjet), ProjectPart(ProjectPart) {};
	int CalcPay()
	{
		return CalcProjPay();
	}
	int CalcProjPay()
	{
		return ProjBudjet*ProjectPart;
	}
};

class Cleaner :public Personal
{
public:
	Cleaner(int ID, string Name, int base, int Work_Unit) :Personal(ID, Name, base, Work_Unit) {};
};

class Driver :public Personal
{
public:
	Driver(int ID, string Name, int base, int Work_Unit) :Personal(ID, Name, base, Work_Unit) {};
};

class Programmer :public Engineer
{
public:
	Programmer(int ID, string FIO, int Work_Unit, int base, int ProjBudjet, int ProjectPart):
		Engineer(ID, FIO, base, Work_Unit, ProjBudjet, ProjectPart) {};
};

class Tester :public Engineer
{
public:
	Tester(int ID, string FIO, int Work_Unit, int base, int ProjBudjet, int ProjectPart) :
		Engineer(ID, FIO, base, Work_Unit, ProjBudjet, ProjectPart) {};
};

class ProjectManager :public Heading, public Manager
{
protected:
	int SubordPay;
	int SubordinatesNum;
public:
	ProjectManager(int ID, string FIO, int Work_Unit, int ProjBudjet, int ProjectPart, int SubordNum) :
		Manager(ID, FIO, Work_Unit, ProjBudjet, ProjectPart), SubordinatesNum(SubordNum),
		SubordPay(300) {};
	int CalcPay()
	{
		return CalcHeadPay() + CalcProjPay();
	}
	int CalcHeadPay()
	{
		return SubordinatesNum*SubordPay;
	}
};

class TeamLeader :public Programmer, public Heading
{
	int SubordPay;
	int SubordinatesNum;
public:
	TeamLeader(int ID, string FIO, int Work_Unit, int base, int ProjBudjet, int ProjectPart, int SubordNum) :
		Programmer(ID, FIO, Work_Unit, base, ProjBudjet, ProjectPart), SubordinatesNum(SubordNum),
		SubordPay(500){};
	int CalcPay()
	{
		return CalcBasePay() + CalcHeadPay() + CalcProjPay();
	}
	int CalcHeadPay()
	{
		return SubordinatesNum*SubordPay;
	}
};

class SeniorManager :public ProjectManager
{
public:
	SeniorManager(int ID, string FIO, int Work_Unit, int ProjBudjet, int ProjectPart, int SubordNum) :
		ProjectManager(ID, FIO, Work_Unit, ProjBudjet, ProjectPart, SubordNum)
	{
		this->SubordPay = 500;
	};
};